---
title : "A SpaceDot R Framework"
description: "Meta-Analysis is a WIP R framework meant to analyze gene expression datasets from NASA experiments and combine them to select key genes to study in-orbit."
lead: "Meta-Analysis is a WIP R framework meant to analyze gene expression datasets from NASA experiments and combine them to select key genes to study in-orbit."
date: 2020-10-06T08:47:36+00:00
lastmod: 2020-10-06T08:47:36+00:00
draft: false
images: []
---
