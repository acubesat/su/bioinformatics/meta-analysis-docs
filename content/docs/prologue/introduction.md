---
title: "Introduction"
description: "Meta-Analysis is a WIP R framework meant to analyze gene expression datasets from NASA experiments and combine them to select key genes to study in-orbit."
lead: "Meta-Analysis is a WIP R framework meant to analyze gene expression datasets from NASA experiments and combine them to select key genes to study in-orbit."
date: 2020-10-06T08:48:57+00:00
lastmod: 2020-10-06T08:48:57+00:00
draft: false
images: []
menu:
  docs:
    parent: "prologue"
weight: 100
toc: true
---

## TODO

{{< alert icon="👉" text="The below is TODO text." >}}

I am a TODO text.
