---
title: "FAQ"
description: "Answers to frequently asked questions."
lead: "Answers to frequently asked questions."
date: 2020-10-06T08:49:31+00:00
lastmod: 2020-10-06T08:49:31+00:00
draft: false
images: []
menu:
  docs:
    parent: "help"
weight: 630
toc: true
---

## Keyboard shortcuts for search?

- focus: `/`
- select: `↓` and `↑`
- open: `Enter`
- close: `Esc`

## Other documentation?

- [renv](https://rstudio.github.io/renv/reference/index.html)
- [Docker](https://docs.docker.com/)
- [GitLab CI](https://docs.gitlab.com/ee/ci/)

## Can I get support?

Let us know:

- [GitLab Issues](https://gitlab.com/acubesat/su/bioinformatics/meta-analysis/issues/)
- [GitHub Issues](https://github.com/AcubeSAT/meta-analysis/issues)
- [GitHub Discussions](https://github.com/AcubeSAT/meta-analysis/discussions/)
- [Gitter Community](https://gitter.im/acubesat-meta-analysis/community)

## Contact the creator?

Send `@xlxs4` or `@esandal` a message:

- [Gitter Community](https://gitter.im/acubesat-meta-analysis/community)
